# SPDX-License-Identifier: MIT

# Only used in https://gitlab.com/fedora/ostree/ci-test
# For tests running in the Fedora infrastructure, see .zuul.yaml and
# https://fedoraproject.org/wiki/Zuul-based-ci

# See: https://gitlab.com/fedora/ostree/buildroot
image: quay.io/fedora-ostree-desktops/buildroot:rawhide

stages:
  - build
  - merge

# As those are not official images, we build all available variants
.parallel:
  parallel:
    matrix:
    - VARIANT:
      - silverblue
      - kinoite
      - kinoite-mobile
      - sway-atomic
      - xfce-atomic
      - lxqt-atomic
      - budgie-atomic
      - base-atomic
      - cosmic-atomic

# Only build the images for merge requests
build-mr-x86_64:
  stage: build
  script:
    - just compose-image $VARIANT
  parallel: !reference [.parallel, parallel]
  tags:
    - saas-linux-small-amd64
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

build-mr-aarch64:
  stage: build
  script:
    - just compose-image $VARIANT
  parallel: !reference [.parallel, parallel]
  tags:
    - saas-linux-small-arm64
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

# Build and upload the images for commits pushed to the branch & scheduled pipelines
build-x86_64:
  stage: build
  script:
    - just compose-image $VARIANT
    - just upload-container $VARIANT x86_64
  parallel: !reference [.parallel, parallel]
  tags:
    - saas-linux-small-amd64
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && ($CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "schedule")

build-aarch64:
  stage: build
  script:
    - just compose-image $VARIANT
    - just upload-container $VARIANT aarch64
  parallel: !reference [.parallel, parallel]
  tags:
    - saas-linux-small-arm64
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && ($CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "schedule")

merge:
  stage: merge
  script:
    - just multi-arch-manifest $VARIANT
    - just sign $VARIANT
  parallel: !reference [.parallel, parallel]
  needs: ["build-x86_64", "build-aarch64"]
  tags:
    - saas-linux-small-amd64
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && ($CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "schedule")
